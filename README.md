# Frontend

This is the frontend of our project, it is a simple HTML5, CSS3, JS site. 
It connects with the backend and pulls data via Firebase. 

It runs publicly at http://gml-atthack19.netlify.com/
