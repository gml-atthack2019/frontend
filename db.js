colors = {};

niceColors = [
  "rgba(39,208,4,0.98)",
  "rgba(24,37,246,0.9)",
  "rgba(238,8,14,0.95)",
  "rgba(210,205,0,0.9)",
  "rgb(245, 66, 245)",
  "rgb(100,2,139)",
  "rgb(156,161,245)",
  "rgb(129,62,42)",
  "rgb(42,42,42)",
  "rgb(212,120,0)"
];
niceColorId = 0;

function getColorFromSpeakerTag(tag) {
  if (colors[tag] == undefined) {
    if (niceColorId >= niceColors.length) {
      console.log("Ran out of colors");
      colors[tag] = "#000000";
    } else {
      colors[tag] = niceColors[niceColorId];
      niceColorId++;
    }
  }
  return colors[tag];
}
function deleteHistory(e) {
  e.preventDefault();
  let db = firebase.firestore();
  db.collection("rooms")
    .doc(roomId)
    .set({});
}
function confirmDelete(e) {
  if (confirm("Are you sure?")) {
    deleteHistory(e);
  }
}
var audio = document.getElementById("audio");
var seekTime = 0;
var endTime = 0;
audio.addEventListener("loadeddata", function() {
  if (audio.readyState >= 2) {
    audio.play();
    document.getElementById("pause-button").style.visibility = "visible";
    audio.currentTime = seekTime;
  }
});
audio.addEventListener("play", function() {
  console.log("playing");
  document.getElementById("pause-button").style.visibility = "visible";
});
audio.addEventListener("timeupdate", function() {
  if (endTime > 0 && audio.currentTime > endTime + 1) {
    audio.pause();
    document.getElementById("pause-button").style.visibility = "hidden";
  }
});
document.getElementById("pause-button").addEventListener("click", function() {
  audio.pause();
  document.getElementById("pause-button").style.visibility = "hidden";
});
function playAudio(srcUrl, time, time2) {
  seekTime = time;
  endTime = time2;
  if (document.getElementById("audio-source").src != srcUrl) {
    document.getElementById("audio-source").src = srcUrl;
    audio.load();
  } else {
    audio.currentTime = seekTime;
    if (audio.paused) {
      audio.play();
    }
  }
}

document.getElementById("chat").onkeypress = function(e) {
  if (!e) return;
  var keyCode = e.keyCode || e.which;
  if (keyCode === 13) {
    // Enter pressed
    send();
  }
};

// Your web app's Firebase configuration
var firebaseConfig = {
  apiKey: "AIzaSyAMkCzht7TEoiTTsPdv0hGMWbsaXufZLgo",
  authDomain: "atthack2019.firebaseapp.com",
  databaseURL: "https://atthack2019.firebaseio.com",
  projectId: "atthack2019",
  storageBucket: "atthack2019.appspot.com",
  messagingSenderId: "850406167253",
  appId: "1:850406167253:web:efec65f907e637f579085c"
};

function redirect() {
  localStorage.setItem("nick", document.getElementById("nick").value);
  let roomId = document.getElementById("RoomID").value;
  window.location.replace("#" + roomId);
}

// Initialize Firebase
firebase.initializeApp(firebaseConfig);

function load(roomId) {
  console.log(roomId);

  if (!roomId) {
    document.getElementById("nick").value = localStorage.getItem("nick");
    document.getElementById("messages").style.display = "none";
    document.getElementById("error").style.display = "block";
  } else {
    document.getElementById("messages").style.display = "block";
    document.getElementById("error").style.display = "none";
    document.getElementById("room_name").innerHTML = roomId;
    let contentToSummary = "";
    let db = firebase.firestore();
    db.collection("rooms")
      .doc(roomId)
      .onSnapshot(snapshot => {
        let data = snapshot.data();
        let messagePage = "";
        let userPage = "";
        contentToSummary = "";
        let users = [];
        if (data.messages !== undefined) {
          var messages = data.messages;
          //messages.sort((a, b) => a.startTime - b.startTime);
          messages = messages.filter(it => it.text != "");
          for (let i = 0; i < messages.length; i++) {
            let message = messages[i];
            if (i == 0) {
              messagePage +=
                "<p class='message' style='color: " +
                getColorFromSpeakerTag(message.userid) +
                "'>";
            } else if (
              i > 0 &&
              (messages[i - 1].userid != message.userid || true)
            ) {
              contentToSummary += "\n\n";
              messagePage +=
                "</p><p class='message' style='color: " +
                getColorFromSpeakerTag(message.userid) +
                "'>";
            }
            if (users.indexOf(message.userid) === -1) {
              users.push(message.userid);
            }
            if (message.channelid) {
              messagePage += `<span class="transcript-highlight" onclick="playAudio('http://34.90.229.167:3000/rec-${message.channelid}-in.wav',${message.startTime},${message.endTime})">${message.text} </span>`;
            } else {
              messagePage += `<span>${message.text} </span>`;
            }
            contentToSummary += message.text + " ";
          }
          messagePage += "</p>";
        }
        if (data.transcriptions !== undefined) {
          data.transcriptions.forEach(transcript => {
            messagePage +=
              "<p class='transcript' style='color: " +
              getColorFromSpeakerTag(transcript.userid) +
              "; opacity: 0.5'>" +
              transcript.text +
              "</p>";
          });
        }
        users.forEach(user => {
          var isOnline = data.users && data.users.indexOf(user) !== -1;
          userPage += `
            <div class="user" style="opacity: ${
              isOnline ? 1 : 0.3
            }; color: ${getColorFromSpeakerTag(user)}">
              <i class="fas fa-user mr-2"></i>
              <span>${user}</span>
            </div>
          `;
        });
        document.getElementById("messagesView").innerHTML = messagePage;
        document.getElementById("users").innerHTML = userPage;
        document.getElementById("totalUsers").innerHTML = users.length;
        document
          .getElementById("messagesCardBody")
          .scrollTo(
            0,
            document.getElementById("messagesCardBody").scrollHeight
          );
      });
    setInterval(() => {
      Summary.summarize(null, contentToSummary, function(err, summary) {
        document.getElementById("textSummary").innerHTML = summary;
      });
    }, 5000);
  }
}
let urlParts = window.location.href.split("#");
let roomId = urlParts.length > 1 ? urlParts[1] : null;
load(roomId);
window.onhashchange = function() {
  load(window.location.hash.replace("#", ""));
};

function send() {
  let chatMessage = document.getElementById("chat").value;
  let nick = localStorage.getItem("nick");
  let d = new Date();
  let msg = {
    text: chatMessage,
    userid: "guest-" + nick,
    date: d
  };
  let db = firebase.firestore();
  console.log(msg);
  db.collection("rooms")
    .doc(roomId)
    .update({
      messages: firebase.firestore.FieldValue.arrayUnion(msg)
    });
  document.getElementById("chat").value = "";
}
